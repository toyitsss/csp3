import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

const AppNavBar = () => {

    const {user} = useContext(UserContext)
    return (
        <Navbar bg="" className="appNavBar" variant="dark" expand="lg" style={{backgroundColor: "#1B1B3A"}}>
                   <Link className="navbar-brand" to="/" style={{color: "#E2ADF2"}}>The Vinyl Ventures</Link>
                   <Navbar.Toggle aria-controls="basic-navbar-nav" />
                   <Navbar.Collapse id="basic-navbar-nav">
                       <Nav className="mr-auto">
                           <Link className="nav-link" to="/products">
                               {user.isAdmin === true ?
                                       <span style={{color: "#FBFFF1"}}>Admin Dashboard</span>
                                   :
                                       <span style={{color: "#FBFFF1"}}>Products</span>
                               }  
                           </Link>
                       </Nav>
                       <Nav className="ml-auto">
                           {user.id !== null ?
                                   user.isAdmin === true ?
                                           <Link className="nav-link" to="/logout" style={{color: "#FBFFF1"}}>
                                               Log Out
                                           </Link>
                                       :
                                           <React.Fragment>
                                               <Link className="nav-link" to="/cart" style={{color: "#FBFFF1"}}>
                                                   Cart
                                               </Link>
                                               <Link className="nav-link" to="/orders" style={{color: "#FBFFF1"}}>
                                                   Orders
                                               </Link>
                                               <Link className="nav-link" to="/logout" style={{color: "#FBFFF1"}}>
                                                   Log Out
                                               </Link>
                                           </React.Fragment>
                               :
                                   <React.Fragment>
                                       <Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}} style={{color: "#FBFFF1"}}>
                                           Log In
                                       </Link>
                                       <Link className="nav-link" to="/register" style={{color: "#FBFFF1"}}>
                                           Register
                                       </Link>
                                   </React.Fragment>
                           }              
                       </Nav>
                   </Navbar.Collapse>
               </Navbar>
           );

}

export default AppNavBar;


