import Banner from "../components/Banner"


const ErrorPage = () => {
 
   let errorPage = {
 
       title: "Page Not Found",
       content: "The page your looking for does not exist.",
       label: "Back to Home",
       destination: "/"
 
   }
 
   return <Banner data={errorPage} />
 
}
export default ErrorPage;