import React from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import { Container } from 'react-bootstrap'
import { MDBFooter, MDBContainer, MDBCol, MDBRow, } from 'mdb-react-ui-kit';

 
const Home = () => {
   const pageData = {
       title: "Vinyl Ventures",
       content: "You're #1 Music Shop",
       destination: "/products",
       label: "Browse Products"
   };
 
   return(
    <>
       <React.Fragment>
           <Banner data={pageData}/>
           <Container fluid>
               <h2 className="text-center mb-4">Featured Products</h2>
               <Highlights/>
           </Container>
       </React.Fragment>


       <MDBFooter className='bg-dark text-white'>
      <div className='container p-4'>
        <MDBRow>
          <div className='col-lg-6 col-md-12 mb-4 mb-md-0'>
            <h5 className='text-uppercase'>About Us</h5>

            <p>
              We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us!
            </p>
          </div>

          <div className='col-lg-3 col-md-6 mb-4 mb-md-0'>
            {/* <h5 className='text-uppercase'>Links</h5> */}

            <ul className='list-unstyled mb-0'>
              <li>
                <a href='#!' className='text-white'>
                  Our Story
                </a>
              </li>
              <li>
                <a href='#!' className='text-white'>
                  Our Team
                </a>
              </li>
              <li>
                <a href='#!' className='text-white'>
                  FAQ
                </a>
              </li>
              <li>
                <a href='#!' className='text-white'>
                  Contact Us
                </a>
              </li>
            </ul>
          </div>

          <div className='col-lg-3 col-md-6 mb-4 mb-md-0'>
            {/* <h5 className='text-uppercase mb-0'>Social Media</h5> */}

            <ul className='list-unstyled'>
              <li>
                <a href='#!' className='text-white'>
                  Facebook
                </a>
              </li>
              <li>
                <a href='#!' className='text-white'>
                  Instagram
                </a>
              </li>
              <li>
                <a href='#!' className='text-white'>
                  Twitter
                </a>
              </li>
              <li>
                <a href='#!' className='text-white'>
                  Youtube
                </a>
              </li>
            </ul>
          </div>
        </MDBRow>
      </div>

      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        © 2022 <br />
        <a className='text-white' href='/'>
          I A N  F A M I L A R A
        </a>
      </div>
    </MDBFooter>
    </>
   );
}
export default Home;